import React from 'react';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer,
  createSwitchNavigator,
  createDrawerNavigator
} from 'react-navigation';
import {Image,
    View,
    TouchableOpacity,
    NativeModules,
    AsyncStorage} from "react-native";
import HomeScreen from "./src/components/HomeScreen"
import DoctorRegisterScreen from "./src/components/doctor/DoctorRegisterScreen"
import LoginScreen from "./src/components/LoginScreen";
import DoctorHomeScreen from "./src/components/doctor/DoctorHomeScreen";
import DoctorDemandesScreen from "./src/components/doctor/DoctorDemandesScreen";
import AccountTypeScreen from "./src/components/AccountTypeScreen"
import PatientRegisterScreen from "./src/components/patient/PatientRegisterScreen";
import PatientHomeScreen from "./src/components/patient/PatientHomeScreen";
import PatientRVScreen from "./src/components/patient/PatientRVScreen";
import AuthLoaderScreen from "./src/components/loader/loader"
import drawerContentComponents from "./src/components/drawerContentComponents";

console.reportErrorsAsExceptions = false;


class DoctorMenu extends React.Component{
  switch(){
      navigation.navigate('toggleDrawer');
  }
  render(){
    return(
        <View>
          <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
            <Image style={{width:40,height:40}} source={require('./src/icons/menu.png')} />
          </TouchableOpacity>
        </View>
    )
  }
}

export class LogOut extends React.Component{
    constructor(props){
        super(props)
    }

    logOut(){
        AsyncStorage.removeItem('token')
        this.props.navigation.navigate("Login");
    }

    render(){
        return(
            <View>
                <TouchableOpacity onPress={() => this.logOut()}>
                  <Image style={{width:30,height:30}} source={require('./src/icons/logout.png')} />
                </TouchableOpacity>
            </View>
        )
    }
}

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
      navigationOptions: {
          header:null,
      }
  },AccountTypeScreen:{
    screen:AccountTypeScreen,
        navigationOptions: {
            header:null,
        }
  },
    DoctorRegister: {
        screen: DoctorRegisterScreen,
        navigationOptions: {
            header:null,
        }
    },
    PatientRegister: {
        screen: PatientRegisterScreen,
        navigationOptions: {
            header:null,
        }
    },
  Login:{
    screen: LoginScreen,

  },
},
    {navigationOptions: {
        header:null,
        tabBarVisible:false
}});

const DoctorStack = createStackNavigator({
    DoctorHome:{
    screen:DoctorHomeScreen,
    navigationOptions: ({ navigation }) => ({
            headerLeft:<DoctorMenu navigation={navigation}/>,
            headerRight:<LogOut navigation={navigation}/>,
            headerTitle:"Informations "
        })
  }
},/*{
    contentComponent:drawerContentComponents
}*/)

const OtherStack = createStackNavigator({
  // /*  Home:{screen:HomeScreen,
  //       navigationOptions: ({ navigation }) => ({
  //           header:null,
  //       })},*/
    Login:{screen:LoginScreen,
        navigationOptions: ({ navigation }) => ({
            header:null,
        })},DoctorRegister: {
        screen: DoctorRegisterScreen,
        navigationOptions: {
            header:null,
        }
    },
    PatientRegister: {
        screen: PatientRegisterScreen,
        navigationOptions: {
            header:null,
        }
    },AccountTypeScreen:{
        screen:AccountTypeScreen,
        navigationOptions: {
            header:null,
        }
    }
})

const ListDemandeStack = createStackNavigator({
    DoctorDemandesScreen: {
        screen:DoctorDemandesScreen,
        navigationOptions: ({ navigation }) => ({
            headerLeft:<DoctorMenu navigation={navigation}/>,
            headerRight:<LogOut navigation={navigation}/>,
            headerTitle:"Demandes "
        })
    }
});

const MyDrawerNavigator = createDrawerNavigator({
    TestMenu:{
        screen:ListDemandeStack,
        navigationOptions: ({ navigation }) => ({
            headerLeft:<DoctorMenu navigation={navigation}/>,
            headerRight:<LogOut/>,
            drawerLabel: 'Demandes Immediates'
        })
    },
    "Envoyer Infos": {
        screen:DoctorStack,
        navigationOptions: ({ navigation }) => ({
            headerLeft: <DoctorMenu navigation={navigation}/>,
            headerRight:<LogOut/>,
            drawerLabel:"Envoyer Infos",
            tabBarVisible:true,
            headerTitle:"Demandes Immediates"
        })
    },

},{
    contentComponent:drawerContentComponents
});

const PatientStack = createStackNavigator({
    PatientHome:{
        screen:PatientHomeScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle:"Prise de RDV",
            headerRight:<LogOut navigation={navigation}/>,
            tabBarVisible:true,

        }),

    },
    PatientRV: DoctorRegisterScreen,
});

const patientNavigator = createBottomTabNavigator({
    "Prendre RDV":{
        screen:PatientStack,
        navigationOptions:{
            tabBarIcon : () => {
                return <Image source={require('./src/icons/calendar-empty.png')} style={{width:20,height:20}}/>
            }
        }
    },
    "A Venir":PatientRVScreen,
    "Passer":PatientHomeScreen
},{
    tabBarOptions:{
        activeTintColor: '#20E3B7',
    }
})
/*
const b = createBottomTabNavigator({
    Home:{screen: DoctorStack} ,
    Default: DoctorRegisterScreen,
    Test:HomeStack
})*/

const AppContainer = createAppContainer(createSwitchNavigator( {
    Other:{screen:OtherStack},
    DoctorMenu:MyDrawerNavigator,
    Patient:patientNavigator
       // Doctor:b
    }));

export default class App extends React.Component {
  render() {
    return(
        <AppContainer />
    )
  }
}
