import React from "react"
import {
    View,
    Text,
    StyleSheet, Image, TouchableOpacity,
} from "react-native"


export default class drawerContentComponents extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.profil}>
                    <Image style={{width:'100%',height:"50%",flex:2}}  resizeMode="contain" source={require('../icons/icons-user.png')} />
                    <Text style={styles.userName}>Diadji Ndiaye</Text>
                </View>
                <View style={styles.nav}>
                    <Text style={styles.item} onPress={() => this.props.navigation.navigate('TestMenu')}>Demandes Immediates</Text>
                    <Text style={styles.item} onPress={() => this.props.navigation.navigate('Envoyer Infos')}>Mes Infos</Text>
                    <Text style={styles.item} onPress={() => alert('Not Implemented')}>Paiement</Text>
                    <Text style={styles.item} onPress={() => alert('Not Implemented')}>Guide / Aide</Text>
                    <Text style={styles.item} onPress={() => alert('Not Implemented')}>Paramétres</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        width:'100%',
    },
    profil:{
        padding:'2%',
        height:'40%',
        backgroundColor:'#000',
    },
    nav:{
        marginTop:'2%',
        marginLeft:'5%',
        padding:'5%',

    },
    item:{
        color:'#000',
        fontSize:16,
        marginBottom:'10%'
    },
    userName:{
        color:'#fff',
        fontWeight: 'bold',
        textAlign:'center',
        marginTop: '2%'
    }
})