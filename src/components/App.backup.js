import React from 'react';
import {createStackNavigator, createBottomTabNavigator,createAppContainer} from 'react-navigation';
import {Image,View} from "react-native";
import HomeScreen from "./src/components/HomeScreen"
import DoctorRegisterScreen from "./src/components/DoctorRegisterScreen"
import LoginScreen from "./src/components/LoginScreen";
import DoctorHomeScreen from "./src/components/DoctorHomeScreen"
import AccountTypeScreen from "./src/components/AccountTypeScreen"
import PatientRegisterScreen from "./src/components/PatientRegisterScreen";

class LogoTitle extends React.Component{

  render(){
    return(
        <Image style={{width:'40%',height:'100%'}} source={require('./src/icons/logo-small.png')} />
    )
  }
}

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      header: null
    }
  },AccountTypeScreen:{
    screen:AccountTypeScreen,
    navigationOptions: {
      header:null,
    }
  },
  Login:{
    screen: LoginScreen,
    navigationOptions:{
      header:null,
    },
  },


});

const DoctorStack = createStackNavigator({
  DoctorRegister: {
    screen: DoctorRegisterScreen,
    navigationOptions: {
      header:null,
    }
  },DoctorHome:{
    screen:DoctorHomeScreen,
    navigationOptions:{
      headerTitle: null,
    }
  }
})

const PatientStack = createStackNavigator({
  PatientRegister: {
    screen: PatientRegisterScreen,
    navigationOptions: {
      header:null,
    }
  }
});

const AppContainer = createAppContainer(createBottomTabNavigator({
  Home:{
    screen:HomeStack,
    navigationOptions: {
      tabBarVisible:false
    }
  },
  Doctor:{
    screen:DoctorStack,
    navigationOptions: {
      tabBarVisible:false
    }
  },
  Patient:{
    screen:PatientStack,
    navigationOptions: {
      tabBarVisible:false
    }
  }
}));

export default class App extends React.Component {
  render() {
    return(
          <AppContainer />
    )
  }
}
