import React from "react"

import MapboxGL from '@mapbox/react-native-mapbox-gl';
import {
    View,
    StyleSheet,
    Text,
    Image,
    TouchableOpacity,
    AsyncStorage, Modal, BackHandler, DeviceEventEmitter,
} from 'react-native';

import axios from "axios"
import {URLAPI} from "../constantes"
import Loader from "../loader/loader"
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import geolocation from "react-native-geolocation-service";
import {LogOut} from "../../../App";

MapboxGL.setAccessToken("pk.eyJ1Ijoib3N0ZW9uYXBwIiwiYSI6ImNqdjJ3cXdubTBhM3M0NG1wMXc0dXplZ3YifQ.xGnDmwtY6BeLVapcAmJjHQ");
const coordinates = [
    [-73.98330688476561, 40.76975180901395],
    [-73.96682739257812, 40.761560925502806],
    [-74.00751113891602, 40.746346606483826],
    [-73.95343780517578, 40.7849607714286],
    [-73.99017333984375, 40.71135347314246],
    [-73.98880004882812, 40.758960433915284],
    [-73.96064758300781, 40.718379593199494],
    [-73.95172119140624, 40.82731951134558],
    [-73.9829635620117, 40.769101775774935],
    [-73.9822769165039, 40.76273111352534],
    [-73.98571014404297, 40.748947591479705]
]

export default class PatientHomeScreen extends React.Component{
    constructor (props) {
        super(props);

        this.state = {
            coordinates: coordinates,
            selectedCity:'',
            token:'',
            loading:false,
            error:false,
            visible:false,
            locationModalVisible:false,
        };
       // this.checkIsLocation().catch(error => error);
        this.testFalse.bind(this);
        this.testTrue.bind(this);
       this.getLocation();
      /*  BackHandler.addEventListener('hardwareBackPress', () => {
        });
        DeviceEventEmitter.addListener('locationProviderStatusChange', function(status) { // only trigger when "providerListener" is enabled
            console.log("location event")
            console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}

            if(status.enabled){
                this.testFalse();
            }else{
                this.testTrue();
            }
        });*/
    }

    getAdress(coordinates) {
        console.log("appel")
       const r = `https://api.mapbox.com/geocoding/v5/mapbox.places/${coordinates}.json?access_token=pk.eyJ1Ijoib3N0ZW9uYXBwIiwiYSI6ImNqdjJ3cXdubTBhM3M0NG1wMXc0dXplZ3YifQ.xGnDmwtY6BeLVapcAmJjHQ`
        axios.get(r).then(response =>{
            console.log(response.data.features[0].place_name)
            this.setState({selectedCity:response.data.features[0].place_name})
        }).catch(error => console.log(error))
    }

    static navigationOptions =  {
       title:"Prise de RDV",
        tabBarIcon : () => {
            return <Image source={require('../../icons/calendar-empty.png')} style={{width:20,height:20}}/>
        }
    }

    renderAnnotationPoint (counter) {
        const id = `pointAnnotation${counter}`;
        const coordinate = this.state.coordinates[counter];
        const title = `Longitude: ${this.state.coordinates[counter][0]} Latitude: ${this.state.coordinates[counter][1]}`;

        return (
            <MapboxGL.PointAnnotation
                key={id}
                id={id}
                title={title}
                coordinate={coordinate}
                onSelected={(e) => console.log(e)}>
                <TouchableOpacity onPress={() => {this.getAdress(coordinate)}}>
                    <Image
                        source={require('../../icons/marker.png')}
                        style={{
                            flex: 1,
                            resizeMode: 'contain',
                            width: 25,
                            height: 25
                        }}/>
                </TouchableOpacity>
            </MapboxGL.PointAnnotation>
        );
    }

    renderAnnotations () {
        const items = [];

        for (let i = 0; i < this.state.coordinates.length; i++) {
            items.push(this.renderAnnotationPoint(i));
        }

        return items;
    }

    makeAppointment(){
        this.setState({loading:true})
        fetch(URLAPI+'/patient/make-appointment', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                data:{
                    city:this.state.selectedCity,
                    token:this.state.token
                }
            }),}).then( response => response.json()
        ).then(responseJSON =>{
            console.log(responseJSON)
            let success = responseJSON.success;
            if(success){
                this.setState({visible:true});
            }else{
                alert("Une erreur s'est produite lors de la creation du RDV")
            }
            this.setState({loading:false})
        }).catch( (error) => {
            console.log(error)
        });
    }

    async checkIsLocation():Promise {
        let check = await LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "Qui",
            cancel: "Non",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
            preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
            providerListener: true // true ==> Trigger locationProviderStatusChange listener when the location state changes
        }).then(function (success) {
            console.log("ok")
            console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
            if(success.enabled){
                console.log("note enabled")
                this.testFalse();
            }else{
                console.log("enabled")
                this.testTrue();
            }
            //this.props.navigation.navigate('DoctorHomeScreen')
        }).catch((error) => {
//            this.props.navigation.navigate('DefaultScreen')
            console.log(error)
            //this.testTrue();
        });

        return Object.is(check.status, "enabled");
    }

    testFalse(){
        this.setState({locationModalVisible:false})
    }

    testTrue(){
        this.setState({locationModalVisible:true})
    }

    getLocation = () => {
       try {
            geolocation.getCurrentPosition(
                (position) => {
                   console.log(position)
                   //this.testFalse()
                },
                (error) => {
                    this.testTrue();
                    console.log(error)
                    console.log("Vous devez activer la geolocalisation pour utiliser l'application 2")
                    return false;
                },
              /*  {enableHighAccuracy: true, timeout: 5000, maximumAge: 10000}*/
            );
        } catch (error) {
            alert("Vous devez activer la geolocalisation pour utiliser l'application 3")
            return false;
        }

    }

    componentWillMount() {
        console.log("component will mount")
        AsyncStorage.getItem('token').then((token) => {
            if(token !== null){
                this.setState({
                    token:token
                });
            }else{
                this.props.navigation.navigate("LoginScreen");
            }
        })
    }

    render () {
        return (
            <View style={styles.container}>
               <MapboxGL.MapView
                    style={{flex: 1}}
                    centerCoordinate={[11.256, 43.770]}
                    zoomLevel={5}
                    onPress={(e) => {this.getAdress(e.geometry.coordinates)}}
                    showUserLocation={true}
                    userTrackingMode={1}
                    >
                   {this.renderAnnotations()}
                </MapboxGL.MapView>
                <Loader loading={this.state.loading}/>
                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible={this.state.visible}
                    onRequestClose={() => {console.log('close modal')}}>
                    <View style={styles.modalBackground}>
                        <View style={styles.modalContent}>
                            <Text style={styles.title}>Message</Text>
                            <Text>{"La demande de consultation à l'adresse "+ this.state.selectedCity +" a bien été envoyée."}</Text>
                            <TouchableOpacity style={styles.closeBtn} onPress={() => this.setState({visible:false})}>
                                <Text style={styles.textBtn}>Fermer</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>

                <Modal
                    transparent={true}
                    animationType={'fade'}
                    visible={this.state.locationModalVisible}
                    onRequestClose={() => {console.log('close modal')}}>
                    <View style={styles.modalBackgroundRv}>
                        <View style={styles.modalContentRv}>
                            <View style={styles.modalHeaderRv}>
                                    <Text style={styles.titleRv}>Prendre mon rendez-vous</Text>
                            </View>
                            <View >
                                <Text style={{color:'#fff', borderBottomColor:'#fff', borderBottomWidth:1,paddingBottom:'2%',fontWeight:'200'}}>
                                    {"Votre lieu de prise en charge doit être déterminé par la géolocalisation pour des raisons de sûreté. Activez là dans les paramétres."}</Text>
                                <View style={{flex:1,flexDirection:'row',paddingTop:'0.5%',}}>
                                    <TouchableOpacity style={styles.closeBtn} onPress={() => this.testFalse()}>
                                        <Text style={styles.textBtn}>Activer la localisation</Text>
                                    </TouchableOpacity>
                                    <Image source={require('../../icons/custom_pin.png')} style={{width:35,height:35,}}/>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>

                <View style={styles.choice}>
                    <View style={{flex:1,fontSize:28,flexDirection:'row', borderTopStartRadius:10,
                        borderWidth:1,borderTopEndRadius:10,//height:'8%',
                        borderColor:"#bfbfbf"}}>
                        <TouchableOpacity style={styles.btnNow} onPress={() => this.makeAppointment()}>
                            <Text style={{ fontSize:18,
                                fontWeight:'300',
                                color:"#fff",}}>
                                Maintenant
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.btnAfter}>
                            <Text style={{ fontSize:18,
                                fontWeight:'300',
                                color:"#dfe0e2",}}>Plus Tard</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex:0.5,flexDirection:'column',
                        borderTopWidth:0.5,borderWidth:1,
                        borderColor:"#bfbfbf",padding: '2%',height: 10}}>
                        <Text style={{marginRight:5}}>{this.state.selectedCity}</Text>
                    </View>
                    <View style={{flex:2,flexDirection:"row",marginTop:'5%',justifyContent:'space-around',width:'100%'}}>
                        <View>
                            <Image source={require('../../icons/event.png')} style={{width:50,height:50}}/>
                            <Text>Adresse 1</Text>
                        </View>
                        <View>
                            <Image source={require('../../icons/location.png')} style={{width:50,height:50}}/>
                            <Text>Domicile</Text>
                        </View>
                        <View>
                            <Image source={require('../../icons/event.png')} style={{width:50,height:50}}/>
                            <Text>Adresse 2</Text>
                        </View>
                        <View>
                        <Image source={require('../../icons/event.png')} style={{width:50,height:50}}/>
                        <Text>Adresse 3</Text>
                    </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1
    },
    choice:{
        flex:1,
        flexDirection:'column',
        justifyContent:'space-around',
        marginTop:'5%',
        paddingLeft:'8%',
        paddingRight:'8%',
       // alignItems:'center'
    },
    btnNow:{
        flex:1,
        backgroundColor:'#20E3B7',
      //  padding:'2%',

        borderColor:"#bfbfbf",
        borderTopStartRadius:6,
        //paddingLeft:'10%',
        alignItems:'center',
        justifyContent:'space-around',
    },
    btnAfter: {
        flex:1,
        //paddingLeft:'10%',
       // padding:'5%',
        alignItems:'center',
        justifyContent:'space-around',
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    modalBackgroundRv: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        paddingTop:'20%',
    },
    modalContent: {
        backgroundColor: '#FFFFFF',
        height: 150,
        width: 300,
        borderRadius: 10,
        //marginTop:'20%',
        //display: 'flex',
        //alignItems: 'center',
        //justifyContent: 'space-around',
        padding:'2%'
    },
    modalContentRv: {
        backgroundColor: '#2775A6',
        height: '45%',
        width: 300,
        borderRadius: 8,
        //paddingTop:'0%',
        display: 'flex',
        alignItems: 'center',
      //  justifyContent: 'space-around',
        paddingBottom:'2%',
        paddingLeft:'2%',
        paddingRight:'2%',
    },
    title:{
        fontSize:18,
        fontWeight:'bold',
        color:"#000"
    },
    closeBtn:{
        flex:2,
        marginTop:'5%',
        marginBottom:'2%'
    },
    textBtn:{
        color:"#fff",
        fontSize:16,
        fontWeight:'bold',
    },
    titleRv:{
         fontSize:18,
        fontWeight:'bold',
        color:"#000",
        width:'100%',
        textAlign: 'center',
        marginBottom:'10%',
    },
    modalHeaderRv: {
        backgroundColor:'#fff',
        justifyContent: 'space-around',
        width: 300,
        paddingTop:'5%',
        marginTop:0,
        marginBottom:'3%',
        borderBottomLeftRadius:5,
        borderBottomRightRadius:5,
    },
})