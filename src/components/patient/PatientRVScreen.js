import React from "react"
import {
    View,
    Text, Image
} from "react-native"

export default class PatientRVScreen extends React.Component{

    constructor(props){
        super(props)
    }

    static navigationOptions =  {
        tabBarIcon : () => {
            return <Image source={require('../../icons/a-venir.png')} style={{width:20,height:20}}/>
        }
    }

    render(){
        return(
            <View>
                <Text>Rendez Vous A Venir</Text>
            </View>
        )
    }
}