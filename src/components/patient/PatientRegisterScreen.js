import React from "react"
import {
    TextInput,
    Text,
    Divider
} from "react-native-paper"
import {
    ScrollView,
    Image,
    StyleSheet,
    View,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native'
import globalStyle from "../../styles/GlobalStyle"
import Loader from "../loader/loader"
import {URLAPI} from "../constantes"

export  default class PatientRegisterScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            sexe:'',
            age:'',
            loading:false,
        }
        this.submit.bind(this)
    }

    submit(){
        this.showLoader();
        console.log("Inscription en cours de traitement...");
        fetch(URLAPI+'/patient-register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                data:this.state
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.redirectUser(responseJson.success)
                this.hideLoader();
            })
            .catch((error) => {
                this.hideLoader();
                console.error(error);
            });
    }

    redirectUser(response){
        if(response == true){
            this.props.navigation.navigate("Login");
        }else{
            alert("Une Erreur s'est produite de la creation de compte");
        }
    }

    showLoader(){
        this.setState({loading:true})
    }

    hideLoader(){
        this.setState({loading:false})
    }
    setLastName(lastName){
        this.setState({lastName})
    }

    setFirstName(firstName){
        this.setState({firstName})
    }

    setEmail(email){
        this.setState({email})
    }

    setPassword(password){
        this.setState({password})
    }

    setSexe(sexe){
        this.setState({
            sexe:sexe
        })
    }

    setAge(age){
        this.setState({
            age:age
        })
    }

    render(){
        return(
            <ScrollView>
                <View style={styles.header}>
                    <Text style={styles.title}>Création compte</Text>
                    <Text style={styles.title}>Patient</Text>
                    <Image style={{width:100,height:100}} source={require('../../icons/icons-user.png')} />
                </View>
                <Loader loading={this.state.loading}/>
                <View style={styles.form}>
                    <TextInput style={globalStyle.input} label="Email" value={this.state.email} onChangeText={(email) => this.setEmail(email)} theme={globalStyle.inputTheme}/>

                    <TextInput style={globalStyle.input} label="Mot de Passe" secureTextEntry={true} value={this.state.password} onChangeText={(password) => this.setPassword(password)} theme={globalStyle.inputTheme}/>

                    <TextInput style={globalStyle.input} label='Prénom' value={this.state.firstName} onChangeText={(firstName) => this.setFirstName(firstName)} theme={globalStyle.inputTheme}/>

                    <TextInput style={globalStyle.input} label='Nom' value={this.state.lastName} onChangeText={(lastName) => this.setLastName(lastName)} theme={globalStyle.inputTheme}/>

                    <TouchableOpacity style={styles.btnSubmit} onPress={() => this.submit()}>
                        <Text>Créer un Compte</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    inputContainer:{
        marginBottom:20,
        width:'70%',
    },
    container:{
        flex:1
    },
    header:{
        marginTop:20,
        alignItems: 'center',
    },
    title:{
        alignItems: 'center',
        fontSize: 28,
    },
    footer:{
        flex:1,
        marginTop:'2%',
        width:200,
    },
    btnCreatAccount: {
        borderRadius:10,
        padding:10,
        borderColor:'#20E3B7',
        borderWidth:2,
        backgroundColor:'#ffffff',
        width:150,
        alignItems:'center',
    },
    btnContainer:{
        borderColor:'#20E3B7',
        padding:20,
        width:150,
        fontWeight:'bold'
    },
    greenText:{
        fontSize:14,
        color:'#20E3B7',
        alignItems:'center',
        textDecorationLine:'underline',
        textAlign: 'center'
    },
    btnSubmit:{
        width:150,
        borderRadius:10,
        backgroundColor:'#20E3B7',
        marginTop:20,
        padding:10,
        alignItems:'center',
    },
    form:{
        marginTop:0,
        paddingTop:'2%',
        paddingLeft:'5%',
        paddingRight:'5%',
        flex:1,
        alignItems:'center',
    }
})
