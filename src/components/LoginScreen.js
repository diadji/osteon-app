import React from "react";
import {
    TextInput,
    Text
} from "react-native-paper";
import {
    View,
    AsyncStorage,
    StyleSheet,
    Image,
    TouchableOpacity,
} from "react-native";
import globalStyle from "../styles/GlobalStyle";
import Loader from "./loader/loader";
import MessageModal from"./MessageModal";

import {URLAPI} from "./constantes";

export default class LoginScreen extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            email:'',
            password:'',
            loading:false,
            errorLogin:false,
            errorMessage:"Veillez verifier votre login ou mot de passe",
        }
        this.submit.bind(this);
        this.isLogin.bind(this);
    }

    _setEmail(email){
        this.setState({email})
    }

    _setPassword(password){
        this.setState({password})
    }

   submit(){
        this.setState({loading:true})
       fetch(URLAPI+'/signUp', {
           method: 'POST',
           headers: {
               Accept: 'application/json',
               'Content-Type': 'application/json',
           },
           body: JSON.stringify({
              data:{
                  email:this.state.email,
                  password:this.state.password
              }
           }),
       }).then((response) => response.json())
           .then((responseJson) => {
               let auth = responseJson.auth;
               let userType = responseJson.user_type;
               this.setState({loading:false})

               if(auth) {
                   let token = responseJson.token;

                   AsyncStorage.setItem('token', token);
                   AsyncStorage.setItem('user_type',userType);

                   this.redirectUser(userType);
               }else{
                   this.setState({errorLogin:true});
                    console.log(this.state.errorLogin)
               }
           })
           .catch((error) => {
               console.error(error);
               this.setState({loading:false})
           });
   }

   redirectUser(userType){
        switch (userType) {
            case 'patient':
                this.props.navigation.navigate('PatientHome');
                break;
            case 'doctor':
                this.props.navigation.navigate("DoctorHome");
                break;
        }
    }

   isLogin(){
       AsyncStorage.getItem('token').then((token) => {
            if(token !== null){
                AsyncStorage.getItem('user_type').then(type =>{
                    this.redirectUser(type);
                });
            }
        })
   }

   componentWillMount() {
        this.setState({loading:false})
        AsyncStorage.getItem('token').then((token) => {
            if(token !== null){
                let userType = AsyncStorage.getItem('user_type');
                this.redirectUser(userType);
            }
        })
    }

    render(){
        this.isLogin()
            return (
                <View style={styles.container}>
                    <View style={styles.logo}>
                        <Image style={{width: '50%', height: 100}} source={require('../icons/logo.png')}/>
                    </View>
                    <Loader loading={this.state.loading}/>
                   {/* <MessageModal showModal={this.state.errorLogin} message={this.state.errorMessage}/>*/}
                    <View style={globalStyle.form}>
                        {
                          this.state.errorLogin ? <Text style={styles.errorText}>{this.state.errorMessage}</Text>:null
                        }
                        <View style={styles.inputContainer}>
                            <TextInput label='email' theme={globalStyle.inputTheme} style={globalStyle.input}
                                       value={this.state.email} onChangeText={(email) => this._setEmail(email)}/>
                        </View>
                        <View style={styles.inputContainer}>
                            <TextInput label='mot de passe' secureTextEntry={true} theme={globalStyle.inputTheme}
                                       style={globalStyle.input} value={this.state.password}
                                       onChangeText={(password) => this._setPassword(password)}/>
                        </View>
                        <View style={styles.footer}>
                            <View style={styles.btnContainer}>
                                <TouchableOpacity style={styles.btnConn} onPress={() => this.submit()}>
                                    <Text style={{color: '#ffffff'}}>Connexion</Text>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.greenText}>Mot de passe perdu ?</Text>
                            <View style={styles.btnContainer}>
                                <TouchableOpacity style={styles.btnCreatAccount}
                                                  onPress={() => this.props.navigation.navigate('AccountTypeScreen')}>
                                    <Text style={{color: globalStyle.color}}>Créer un compte</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
}

const styles = StyleSheet.create({
    inputContainer:{
        marginBottom:20,
        width:'70%',

    },
    title:{
       paddingLeft:'20%',
        marginBottom: '5%',
        alignItems:'center',
    },
    container:{
        flex:1,
        paddingTop:'5%'
    },
    logo:{
        alignItems: 'center',
    },
    footer:{
        flex:1,
        marginTop:'2%',
        width:200,
    },
    btnCreatAccount: {
        borderRadius:10,
        padding:10,
        borderColor:'#20E3B7',
        borderWidth:2,
        backgroundColor:'#ffffff',
        width:150,
        alignItems:'center',
    },
    btnContainer:{
        borderColor:'#20E3B7',
        padding:20,
        width:150,
        fontWeight:'bold'
    },
    greenText:{
        fontSize:14,
        color:'#20E3B7',
        alignItems:'center',
        textDecorationLine:'underline',
        textAlign: 'center'
    },
    btnConn:{
        width:150,
        borderRadius:10,
        backgroundColor:'#20E3B7',
        padding:10,
        alignItems:'center',
    },
    errorText:{
        color:"red",
    }
})