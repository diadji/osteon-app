import React, {Component} from 'react';
import { Text, TouchableHighlight, View, Alert} from 'react-native';

import Modal from "react-native-modal";
export default class ModalExample extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View>
                <Modal isVisible={true}>
                    <View style={{ height: 100 }}>
                        <Text>I am the modal content!</Text>
                    </View>
                </Modal>
            </View>
        );
    }
}