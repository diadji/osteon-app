import React from "react"

import {
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    ScrollView,
    FlatList,
} from "react-native";

import RDVItem from "../RDVItem"
import {URLAPI} from "../constantes"

export default class DoctorDemandesScreen extends React.Component{
    constructor(props){
        super(props);
        this.state= {
            rdvItems:null
        }
        this.getAllDemande();
    }

    getAllDemande(){
        fetch(URLAPI+'/demandes-rdv/all', {
            method: 'GET',
        }).then((response) => response.json())
            .then((responseJson) => {
                this.setState({rdvItems:responseJson})
                console.log(responseJson);
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render(){
        if(this.state.rdvItems === null){
            return (
                <ActivityIndicator  size={"large"} visible={true}/>
            )
        }else{
           return (
               <View style={styles.container}>

                   <FlatList
                        style={{ flex: 1 }}
                        data = {this.state.rdvItems}
                        keyExtractor = {(item) => item.id}
                        renderItem = {({item}) =>  <RDVItem date={item.date_demande} adresse={item.adresse} price={item.price}/>}
                    />
                </View>
           )
        }
    }

}

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: 10
    }
});