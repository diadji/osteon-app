import React from "react"
import {
    TextInput,
    Text,
    Divider
} from "react-native-paper"
import {

    Image,
    StyleSheet,
    View,
    TouchableOpacity,
    KeyboardAvoidingView, AsyncStorage
} from 'react-native'
import globalStyle from "../../styles/GlobalStyle"
import { Header } from 'react-navigation';
import {URLAPI} from "../constantes"

export  default class DoctorRegisterScreen extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            firstName:'',
            lastName:'',
            email:'',
            password:'',
        }
        this.submit.bind(this)
    }

    submit(){
        console.log("Inscription en cours de traitement...");

        fetch(URLAPI+'/doctor-register', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                data:this.state
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                this.redirectUser(responseJson.success)
            })
            .catch((error) => {
                console.error(error);
            });
    }

    redirectUser(response){
        if(response == true){
            this.props.navigation.navigate("Login");
        }else{
            alert("Une Erreur s'est produite de la creation de compte");
        }
    }

    setLastName(lastName){
        this.setState({lastName})
    }

    setFirstName(firstName){
        this.setState({firstName})
    }

    setEmail(email){
        this.setState({email})
    }

    setPassword(password){
        this.setState({password})
    }

    render(){
        return(
            <KeyboardAvoidingView keyboardVerticalOffset = {Header.HEIGHT + 20} style={styles.container} behavior="padding" enabled>
                <View style={styles.header}>
                    <Text style={styles.title}>Créer un compte</Text>
                    <Text style={styles.title}> ostéopathe</Text>
                    <Divider />
                    <Image style={{width:100,height:100}} source={require('../../icons/icons-user.png')} />
                </View>
                <View style={styles.form}>
                    <TextInput style={globalStyle.input} label="Email" value={this.state.email} onChangeText={(email) => this.setEmail(email)} theme={globalStyle.inputTheme}/>
                    <TextInput style={globalStyle.input} label="Mot de Passe" secureTextEntry={true} value={this.state.password} onChangeText={(password) => this.setPassword(password)} theme={globalStyle.inputTheme}/>
                    <TextInput style={globalStyle.input} label='Prénom' value={this.state.firstName} onChangeText={(firstName) => this.setFirstName(firstName)} theme={globalStyle.inputTheme}/>
                    <TextInput style={globalStyle.input} label='Nom' value={this.state.lastName} onChangeText={(lastName) => this.setLastName(lastName)} theme={globalStyle.inputTheme}/>
                    <TouchableOpacity style={styles.btnSubmit} onPress={() => this.submit()}>
                            <Text>Créer un Compte</Text>
                        </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    inputContainer:{
        marginBottom:20,
        width:'70%',

    },
    container:{
        flex:1
    },
    header:{
        marginTop:20,
        alignItems: 'center',
    },
    title:{
        alignItems: 'center',
        fontSize: 28,
        //marginBottom: 20,
    },
    footer:{
        flex:1,
        marginTop:'2%',
        width:200,
    },
    btnCreatAccount: {
        borderRadius:10,
        padding:10,
        borderColor:'#20E3B7',
        borderWidth:2,
        backgroundColor:'#ffffff',
        width:150,
        alignItems:'center',
    },
    btnContainer:{
        borderColor:'#20E3B7',
        padding:20,
        width:150,
        fontWeight:'bold'
    },
    greenText:{
        fontSize:14,
        color:'#20E3B7',
        alignItems:'center',
        textDecorationLine:'underline',
        textAlign: 'center'
    },
    btnSubmit:{
        width:150,
        borderRadius:10,
        backgroundColor:'#20E3B7',
        marginTop:20,
        padding:10,
        alignItems:'center',
    },
    form:{
        marginTop:0,
        paddingTop:'2%',
        paddingLeft:'5%',
        paddingRight:'5%',
        flex:1,
        alignItems:'center',
    }
})