import React from "react"
import {
    Text,
    View,
    CameraRoll,
    Button,
    Image,
StyleSheet, AsyncStorage,ActivityIndicator
} from "react-native"

import ImagePicker from "react-native-image-picker"
import axios from "axios/index";
import RNFetchBlob from 'react-native-fetch-blob';
import {URLAPI} from "../constantes"
import MessageModal from"../MessageModal";

export default class DoctorHomeScreen extends  React.Component{
    constructor(props){
        super(props);
        this.state = {
            avatarSource : require('../../icons/icon-photo.png'),
            avatarSource2 : require('../../icons/icon-photo.png'),
            diplome:{
                data : null,
                imageType: null,
                imageName: null
            },
            carteIdentite:{
                data : null,
                imageType: null,
                imageName: null
            },
            token:null,
            loading:true,
            display:"flex",
            authorized:false,
            message:"none"
        }
        this.submit.bind(this);
        this.isAuthorized();
    }

    _handleButtonAddDiplomePress = () => {
        const options = {
            title:"Selectionner diplome",
        }

        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if(response.customButton){
                console.log('User tapped custom button: ', response.customButton);
            }else{
                const source = { uri: response.uri };
                const res = {
                    path : source,
                    data : response.data
                }
                const diplomeInfo = {
                    data:response.data,
                    imageType:response.type,
                    imageName: response.fileName
                }
                console.log(diplomeInfo);

                this.setState({
                    avatarSource: source,
                    diplome:diplomeInfo,
                });
            }

        });
    };

    _handleButtonAddCINPress = () => {
        const options = {
            title:"Selectionner votre carte d'identite",
        }

        ImagePicker.showImagePicker(options, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if(response.customButton){
                console.log('User tapped custom button: ', response.customButton);
            }else{
                const source = { uri: response.uri };

                const carteIdentiteInfo = {
                    data:response.data,
                    imageType:response.type,
                    imageName: response.fileName
                }

                this.setState({
                    avatarSource2: source,
                    carteIdentite: carteIdentiteInfo
                });
            }
        });
    };

    submit(){
        console.log("submit function")
         const inf = [
           { name: 'diplome', filename: this.state.diplome.imageName, type: this.state.diplome.imageType, data: this.state.diplome.data },
           { name: 'identite', filename: this.state.carteIdentite.imageName, type: this.state.carteIdentite.imageType, data: this.state.carteIdentite.data },
            {name:"token",data:this.state.token}
        ];

         RNFetchBlob.fetch("POST",URLAPI+"/doctor/add-diplome",
            {
               Authorization : "Bearer access-token",
                otherHeader : "foo",
                 'Content-Type' : 'multipart/form-data'
            }, inf)
            .then((response) => {
                const res = JSON.parse(response.data);

                 if(res.auth){
                   if(res.success) {
                        alert("Les informations ont bien été envoyées");
                     }else{
                        alert("Une erreur s'est produite lors de l'envoi des informations");
                   }
                }else{
                    this.props.navigation.navigate("LoginScreen");
                }
             }).catch(error => console.log(error));

    }

    isAuthorized(){
        axios.get("osteonapp.com/doctor/"+this.state.token+"/infos-is-validated").then((response) => {
            console.log(response)
            res = response.data;

            if(res.exist == true){
                if(res.validated){
                    this.setState({loading:false})
                    alert("Vous avez été accepté en tant que docteur")
                }else{
                    alert("Désolé, vos informations n'ont pas encore été validées")
                    this.setState({message:"flex"});
                    this.setState({display:"none"})
                    this.setState({loading:false});
                }
            }else{
                this.setState({loading:false});
                alert("Veillez envoyer vos informations pour pouvoir utiliser l'application")
            }
        }).catch(error => {
            console.log(error)
           // alert("Veillez verifier votre connexion internet")
        })
    }
    componentWillMount() {
        console.log("component will mount")
        AsyncStorage.getItem('token').then((token) => {
            if(token !== null){
                this.setState({
                    token:token
                });
                this.isAuthorized();
                console.log(this.state.token);
            }else{
                this.props.navigation.navigate("LoginScreen");
            }
        })
    }

    render(){
        return(
            <View style={styles.container}>
                {/*<ActivityIndicator size="large" animating={this.state.loading} />*/}
                <View style={{display:this.state.display}}>
                    <View>
                        <Button title="Ajouter Diplome " onPress={this. _handleButtonAddDiplomePress} />
                        <Image source={this.state.avatarSource} style={{height:50,width:50}} />
                    </View>
                    <View>
                        <Button title="Ajouter Carte d'identite " onPress={this._handleButtonAddCINPress} />
                        <Image source={this.state.avatarSource2} style={{height:50,width:50}} />
                    </View>

                    <Button title="Envoyer" onPress={() => {this.submit()}}/>
                </View>
                <View style={{flex:1,justifyContent:'center',alignItems:'center',padding:20}} >
                    <Text style={{fontSize:30,display:this.state.message}}>
                        Votre compte n'est pas encore activé.Veillez patienter SVP.
                    </Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        paddingTop:'10%',
        alignItems: 'center'
    },
    buttonContainer : {
        alignItems:'center',
        marginBottom: 10,
        marginTop:10,
        width:'50%'
    },
    OU:{
        fontSize:14,
    },
    welcomeText:{
        fontSize: 20,
        marginBottom: '40%',
    }
});
