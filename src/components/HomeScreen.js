import React from "react"
import globalStyle from "../styles/GlobalStyle"
import {
    Text,
    Image,
    View,
    Button,
    StyleSheet
} from "react-native"


export default class HomeScreen extends  React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return(
            <View style={styles.container}>
                <Image style={{width:'70%',height:150}} source={require('../icons/logo.png')} />
                <Text style={styles.welcomeText}>Bienvenue</Text>
                <View style={styles.buttonContainer}>
                    <Button  title="Se Connecter" color={globalStyle.color} onPress={() => this.props.navigation.navigate('Login')}/>
                </View>

            </View>
        )
    }
}

//style
const styles = StyleSheet.create({
    container:{
        paddingTop:'10%',
        alignItems: 'center'
    },
    buttonContainer : {
        alignItems:'center',
        marginBottom: 10,
        marginTop:10,
        width:'50%'
    },
    OU:{
        fontSize:14,
    },
    welcomeText:{
        fontSize: 20,
        marginBottom: '40%',
    }
});

//AppRegistry.registerComponent("osteon_app", () =>  HomeScreen);