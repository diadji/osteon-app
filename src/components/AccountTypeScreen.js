import React from "react"

import {
    View,
    Text,
    ImageBackground,
    TouchableOpacity,
    StyleSheet, Image
} from "react-native"

export default class AccountTypeScreen extends React.Component{
    constructor(props){
        super(props);
        this.goToDoctorRegisterForm.bind(this);
    }

    goToDoctorRegisterForm(){
        this.props.navigation.navigate("DoctorRegister");
    }

    goToPatientRegisterForm(){
        this.props.navigation.navigate("PatientRegister");
    }

    render(){
        return(
            <ImageBackground source={require('../icons/back1.jpeg')} style={{width: '100%', height: '100%'}}>
            <View style={styles.container}>
                <Text style={styles.title}>S'inscrire en tant que </Text>
                <View style={{flex:2}}>
                    <TouchableOpacity style={styles.btnUserType} onPress={() => {this.goToPatientRegisterForm()}}>
                        <Image style={{width:"95%",height:"95%",flex:2}}  resizeMode="contain" source={require('../icons/injury.png')} />
                        <Text style={styles.label}>Patient</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnUserType} onPress={() => {this.goToDoctorRegisterForm()}}>
                        <Image style={{width:"95%",height:"95%",flex:2}} resizeMode="contain" source={require('../icons/doctor.png')} />
                        <Text style={styles.label}>Osteopathe</Text>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                </View>
            </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding: 50
    },
    title:{
        fontSize:28,
        marginBottom:'20%',
        color:'#fff'
    },
    btnUserType:{
        width:"100%",
        borderRadius:10,
        borderColor:'#20E3B7',
        backgroundColor:'rgba(254,251,255,0.44)',
        marginTop:20,
        marginBottom:20,
        padding:10,
        flex:2,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    label:{
        fontSize: 20,
        color:'#20E3B7',
        flex:3,
        marginLeft:'10%'
    }
})