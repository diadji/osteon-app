import React from 'react';
import {
    StyleSheet,
    View,
    Modal,
    Text,
    TouchableOpacity,
} from 'react-native';

export default class MessageModal extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            visibled : false,
            message : this.props.message
        }

        this.closeModal.bind(this);
    }

    closeModal(){
        console.log(this.state.visibled)
        this.setState({visibled :false});
    }

    render(){
        return (
            <Modal
                transparent={true}
                animationType={'slide'}
                visible={this.state.visibled}
                onRequestClose={() => {console.log('close modal')}}>
                    <View style={styles.modalBackground}>
                        <View style={styles.activityIndicatorWrapper}>
                            <Text style={styles.title}>Message</Text>
                            <Text>{this.state.message}</Text>
                            <TouchableOpacity style={styles.closeBtn} onPress={() => this.closeModal()}>
                                <Text>Fermer</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 150,
        width: 300,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    title:{
        fontSize:18,
        fontWeight:'bold',
        color:"#000"
    },
    closeBtn:{
        backgroundColor:"rgba(126,126,126,0.61)",
        borderRadius:10,
        padding:5,
    },
    textBtn:{
        color:"#fff"
    }
});
