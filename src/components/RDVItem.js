import React from "react"

import {
    View,
    Text,
    Image,
    StyleSheet,

} from "react-native"

export default class RDVItem extends  React.Component{
    constructor(props){
        super(props);
       console.log(props)
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={styles.firstView}>
                    <View><Text>{this.props.date}</Text></View>
                    <View><Text>Diadji Ndiaye</Text></View>
                    <Image source={require('../icons/patient.png')} style={{width:50,height:50}}/>
                </View>
                <View style={styles.secondView}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Image source={require('../icons/loca.png')} style={{width:25,height:25}}/>
                        <Text>{this.props.adresse}</Text>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Image source={require('../icons/dist.png')} style={{width:30,height:30}}/>
                        <Text> A 25 minutes</Text></View>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Image source={require('../icons/price.png')} style={{width:30,height:30}}/><Text> {this.props.price}€ </Text>
                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'row',
        padding:'1%',
        alignItems:'center',
       // justifyContent:'center',
        borderBottomWidth:0.5,
        borderBottomColor:'#9b9b9b',
        fontSize:20
    },
    firstView:{
        flex:1,
        flexDirection: 'column',

    },
    secondView:{
        flex:1,
        flexDirection: 'column'
    }
})