import React from "react"

import {
    View,
    Text,
    BackHandler,
    DeviceEventEmitter,
    AsyncStorage,
    Button
} from "react-native"
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import geolocation from "react-native-geolocation-service";
import {location} from "../Location"

export default class DefaultScreen extends React.Component{

    constructor(props){

        super(props)
        this.state = {
            geolocationActived : false
        }

        this.checkLocation.bind(this);
        this.redirectUser.bind(this);
        this.checkIsLocation().catch(error => error);

        BackHandler.addEventListener('hardwareBackPress', () => { //(optional) you can use it if you need it
            //do not use this method if you are using navigation."preventBackClick: false" is already doing the same thing.

        });

        DeviceEventEmitter.addListener('locationProviderStatusChange', function(status) { // only trigger when "providerListener" is enabled
            console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
            alert("Veillez activer la localisation pour continuer")
        });
    }

    redirectUser(){
        this.props.navigation.navigate("DefaultScreen")
    }

    hasLocationPermission = async () => {
        if (Platform.OS === 'ios' ||
            (Platform.OS === 'android' && Platform.Version < 23)) {
            return true;
        }
    }

    async checkIsLocation():Promise {
        let check = await LocationServicesDialogBox.checkLocationServicesIsEnabled({
            message: "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Use GPS, Wi-Fi, and cell network for location<br/><br/><a href='#'>Learn more</a>",
            ok: "Qui",
            cancel: "Non",
            enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
            showDialog: true, // false => Opens the Location access page directly
            openLocationServices: true, // false => Directly catch method is called if location services are turned off
            preventOutSideTouch: false, // true => To prevent the location services window from closing when it is clicked outside
            preventBackClick: false, // true => To prevent the location services popup from closing when it is clicked back button
            providerListener: true // true ==> Trigger locationProviderStatusChange listener when the location state changes
        }).then(function (success) {
            console.log("ok")
            console.log(success); // success => {alreadyEnabled: false, enabled: true, status: "enabled"}
            this.props.navigation.navigate('DoctorHomeScreen')
        }).catch((error) => {
            this.props.navigation.navigate('DefaultScreen')
        });

        return Object.is(check.status, "enabled");
    }

    getLocation = () => {
        let response ;
        try {
            geolocation.getCurrentPosition(
                (position) => {
                    this.setState({geolocationActived :true})
                    console.log(position)
                    alert("Ok geo actived")
                },
                (error) => {
                    console.log(error)
                    alert("Vous devez activer la geolocalisation pour utiliser l'application 2")
                    return false;
                },
                {enableHighAccuracy: true, timeout: 1000, maximumAge: 10000}
            );
        } catch (error) {
            alert("Vous devez activer la geolocalisation pour utiliser l'application 3")
            return false;
           }

        return response;
    }

    checkLocation(){
       this.getLocation()
    }

    componentWillMount(): void {
        AsyncStorage.getItem('token').then((token) => {
            if(token !== null){
                this.props.navigation.navigate("DoctorHome")
            }else{
                this.props.navigation.navigate("Login")
            }
        })
    }

    render(){
        return(
            <View>
                <Text>Activer la localisation</Text>
                <Text>{this.state.geolocationActived}</Text>
               <Button title="continuer" onPress={() => this.checkIsLocation()}/>
            </View>
        )
    }
}