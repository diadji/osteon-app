import { StyleSheet } from 'react-native'

export default {
    color:"#20E3B7",
    container:{
        flex:1,
        flexDirection:"column",
        justifyContent:"space-between",
        alignItems: 'center',
        marginBottom:50
    },
    input:{
        backgroundColor:'#fff',
        width:'100%'
    },
    form:{
        paddingTop:'10%',
        paddingLeft:'5%',
        paddingRight:'5%',
        flex:1,
        alignItems:'center',
        //justifyContent: "center"
    },
    inputTheme: {
        colors: {
            accent: '#5b5b5b',
            primary: '#20E3B7',
        },
        background:"#fff",
        text:"#5b5b5b"
    },
    buttonTheme:{
        colors: {
            primary: '#20E3B7',
            accent: '#ffffff',
        },
        margin:50

    },
    roundedButton:{
        borderRadius:20,
        marginBottom: 50
    }
}

const LoginScreenStyle = StyleSheet.create({
    inputContainer:{
        marginBottom:20,
            width:'70%',
    },
    title:{
        paddingLeft:'20%',
            marginBottom: '5%',
            alignItems:'center',
    }, container:{
        flex:1
    },
    logo:{

        alignItems: 'center',
    },
    footer:{

    }
});

export {LoginScreenStyle}